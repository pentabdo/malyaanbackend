package com.example.service;


import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class WebService {

    public String dateFormating (String date)
    {

        String str1 = date.substring(2,4);
        String str2 = date.substring(5,7);
        String str3 = date.substring(8,10);
        String finalDate = str1+str2+str3;
        return finalDate;
    }

    public String backQrGenerator( String productionDate,int countryId, int versionId ,int varientId , long orgId , int productionLocationId , long orgProductId ,long batchId,long itemId,
                                    int shelfLife, int loyalityPoints ) {

//        String binaryRes = String.format("%10d%4d%10d%17d%10d%14d%014d%017d%010d%010d", Integer.toBinaryString(countryId),
//                Integer.toBinaryString(versionId), Integer.toBinaryString(varientId), Long.toBinaryString(orgId),
//                Integer.toBinaryString(productionLocationId),
//                Long.toBinaryString(orgProductId), Long.toBinaryString(batchId), Long.toBinaryString(itemId),
//                Integer.toBinaryString(shelfLife), Integer.toBinaryString(loyalityPoints));
        String date = String.format("%20s",Integer.toBinaryString(Integer.parseInt(productionDate))).replace(" ","0");
        String a = String.format("%10s",Integer.toBinaryString(countryId)).replace(" ","0");
        String b = String.format("%4s",Integer.toBinaryString(versionId)).replace(" ","0");
        String c = String.format("%10s",Integer.toBinaryString(varientId)).replace(" ","0");
        String d= String.format("%17s",Long.toBinaryString(orgId)).replace(" ","0");
        String e = String.format("%10s",Integer.toBinaryString(productionLocationId)).replace(" ","0");
        String f =  String.format("%14s",Long.toBinaryString(orgProductId)).replace(" ","0");
        String g= String.format("%14s",Long.toBinaryString(batchId)).replace(" ","0");
        String h= String.format("%17s",Long.toBinaryString(itemId)).replace(" ","0");
        String i = String.format("%10s",Integer.toBinaryString(shelfLife)).replace(" ","0");
        String j = String.format("%10s",Integer.toBinaryString(loyalityPoints)).replace(" ","0");
         String binaryRes ="1"+date+a+b+c+d+e+f+g+h+i+j;
        String decimalRes = binayToDicimal(binaryRes);


        return decimalRes;
    }
    public String binayToDicimal(String bin)
    {

        BigInteger powTwo = new BigInteger("1");
        BigInteger bigInteger = new BigInteger("0");
// using the length of the string, start with a counter that is the length
// minus 1, decrement it by 1 until we get to 0
        for (int i = bin.length() - 1; i >= 0; i--) {
            // Get the character at position i in the string
            char currentChar = bin.charAt(i);
            // Check for a "1"
            if (currentChar == '1') {
                bigInteger = bigInteger.add( powTwo);
            }
            powTwo =powTwo.multiply(BigInteger.valueOf(2)) ;
        }

        String res = bigInteger.toString();
        return res;
    }

}
