package com.example.entity.mobile;

public class OrganizationInformation {

    private Long id;
    private String name;

    public OrganizationInformation(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public OrganizationInformation() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
