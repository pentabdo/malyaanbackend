package com.example.entity.mobile;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "action",schema = "malyaan_mobile")
public class Action {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String name;
    private String description;

    @OneToMany(
            mappedBy = "action",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<UserAction> userActionList = new HashSet<>();


    public Action() {
    }

    public Action(String description) {
        this.description = description;
    }

    public Action(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserAction> getUserActionList() {
        return userActionList;
    }

    public void setUserActionList(Set<UserAction> userActionList) {
        this.userActionList = userActionList;
    }




}
