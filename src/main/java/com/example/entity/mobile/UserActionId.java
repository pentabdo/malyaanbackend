package com.example.entity.mobile;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class UserActionId implements Serializable {


    @Column(name = "user_id")
    private long userId;

    @Column(name = "action_id")
    private long actionId;

    public UserActionId() {
    }

    public UserActionId(long userId, long actionId) {
        this.userId = userId;
        this.actionId = actionId;
    }


    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getActionId() {
        return actionId;
    }

    public void setActionId(long actionId) {
        this.actionId = actionId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, actionId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj == null || getClass() != obj.getClass())
            return false;

        UserActionId that = (UserActionId) obj;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(actionId, that.actionId);
    }
}
