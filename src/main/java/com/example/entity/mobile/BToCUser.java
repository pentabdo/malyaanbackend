package com.example.entity.mobile;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user",schema = "malyaan_mobile")
public class BToCUser {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id ;

    @Column(name ="user_name")
    private String userName;
    @Column(name = "phone_number")
    private String phoneNumber;

    @ManyToMany(fetch = FetchType.LAZY,cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    } )
    @JoinTable(name = "user_app",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "app_id")
    )
    private Set<App> registers = new HashSet<App>();



    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<UserAction> tags = new HashSet<>();


    public BToCUser() {
    }

    public BToCUser(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Set<App> getRegister() {
        return registers;
    }

    public void setRegister(Set<App> registers) {
        this.registers = registers;
    }



    public void addRegister(App register){
        this.registers.add(register);
        register.getUsers().add(this);
    }

    public void removeRegister(App register){
        this.registers.remove(register);
        register.getUsers().remove(this);
    }

//    public void addAction(Action action){
//        this.actions.add(action);
//        action.getUsers().add(this);
//    }
//    public void removeAction(Action action){
//        this.actions.remove(action);
//        action.getUsers().remove(this);
//    }


    public Set<App> getRegisters() {
        return registers;
    }

    public void setRegisters(Set<App> registers) {
        this.registers = registers;
    }

    public Set<UserAction> getTags() {
        return tags;
    }

    public void setTags(Set<UserAction> tags) {
        this.tags = tags;
    }
}
