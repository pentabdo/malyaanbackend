package com.example.entity.mobile;


import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "user_action",schema = "malyaan_mobile")
public class UserAction {

    @EmbeddedId
    private UserActionId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("user_id")
    private BToCUser user;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("action_id")
    private Action action;

    private Date happensAt;

    private String app ;

    public UserAction() {
    }

    public UserActionId getId() {
        return id;
    }

    public void setId(UserActionId id) {
        this.id = id;
    }

    public BToCUser getUser() {
        return user;
    }

    public void setUser(BToCUser user) {
        this.user = user;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Date getHappensAt() {
        return happensAt;
    }

    public void setHappensAt(Date happensAt) {
        this.happensAt = happensAt;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, action);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj == null || getClass() != obj.getClass())
            return false;

        UserAction that = (UserAction) obj;
        return Objects.equals(user, that.user) &&
                Objects.equals(action, that.action);
    }
}
