package com.example.entity.mobile;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "app",schema = "malyaan_mobile")
public class App {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String appName;

    @ManyToMany(mappedBy = "registers")
    private Set<BToCUser> users;

    public App() {
    }

    public App(String appName) {
        this.appName = appName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Set<BToCUser> getUsers() {
        return users;
    }

    public void setUsers(Set<BToCUser> users) {
        this.users = users;
    }
}
