package com.example.entity;

import javax.persistence.*;

@Entity
@Table(name="courses")
public class Course {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id ;

    private String name;

    public Course() {
    }

    public Course( String name) {

        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}
