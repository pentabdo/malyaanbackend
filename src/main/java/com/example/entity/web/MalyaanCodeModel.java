package com.example.entity.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name ="mlyan_code")
public class MalyaanCodeModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE )
    private long id;
    @Column(name = "encrypted_code")
    private String qrcode;
    private boolean isbought;
    private boolean isused;

    private String scanned_date;
    private String user_id;
    private String qr_image ;
    private String consumption_date;
    private String last_modification_user_id;
  //  private int transaction_id;
//    private String encrypted_code;

    @ManyToOne(fetch = FetchType.EAGER)
 @JoinColumn(name="transaction_id", nullable=false)
 @JsonIgnoreProperties("malyaanCodeModels")
 private TransactionModel transactionModel;

    public MalyaanCodeModel() {
    }

    public MalyaanCodeModel(String qrcode) {
        this.qrcode = qrcode;

    }

    public MalyaanCodeModel(String qrcode, TransactionModel transactionModel ,boolean isbought,boolean isused) {

        this.qrcode = qrcode;
        this.transactionModel = transactionModel;
        this.isbought=isbought;
        this.isused = isused;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public boolean isIsbought() {
        return isbought;
    }

    public void setIsbought(boolean isbought) {
        this.isbought = isbought;
    }

    public boolean isIsused() {
        return isused;
    }

    public void setIsused(boolean isused) {
        this.isused = isused;
    }

    public String getScanned_date() {
        return scanned_date;
    }

    public void setScanned_date(String scanned_date) {
        this.scanned_date = scanned_date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getQr_image() {
        return qr_image;
    }

    public void setQr_image(String qr_image) {
        this.qr_image = qr_image;
    }

    public String getConsumption_date() {
        return consumption_date;
    }

    public void setConsumption_date(String consumption_date) {
        this.consumption_date = consumption_date;
    }

    public String getLast_modification_user_id() {
        return last_modification_user_id;
    }

    public void setLast_modification_user_id(String last_modification_user_id) {
        this.last_modification_user_id = last_modification_user_id;
    }

//    public int getTransaction_id() {
//        return transaction_id;
//    }
//
//    public void setTransaction_id(int transaction_id) {
//        this.transaction_id = transaction_id;
//    }

//    public String getEncrypted_code() {
//        return encrypted_code;
//    }
//
//    public void setEncrypted_code(String encrypted_code) {
//        this.encrypted_code = encrypted_code;
//    }

    public TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }
}
