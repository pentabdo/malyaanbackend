package com.example.entity.web;


public class QrCodeModel  {


    private int  versionId  ;  //%2d
    private int  countryId;//%3d
    private int variantId ;//%3d
    private long count ;
    // private int orgId;//%6d
    private String organization_name;
    private long productId;//%14d
    private String gtin; //%14d
//    private int batchId;//%6d
    private int shelfLife ; //%3d
    private String productionDate ;//%6d
    private int productionLocation;//%4d
//    private int itemID;//%6d
  //  private int otherId; //%3d

    public QrCodeModel() {
    }

    public QrCodeModel(int versionId, int countryId, int variantId, long count, String organization_name, long productId, String gtin, int shelfLife, String productionDate, int productionLocation) {
        this.versionId = versionId;
        this.countryId = countryId;
        this.variantId = variantId;
        this.count = count;
        this.organization_name = organization_name;
        this.productId = productId;
        this.gtin = gtin;
        this.shelfLife = shelfLife;
        this.productionDate = productionDate;
        this.productionLocation = productionLocation;
    }

    public int getVersionId() {
        return versionId;
    }

    public void setVersionId(int versionId) {
        this.versionId = versionId;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getVariantId() {
        return variantId;
    }

    public void setVariantId(int variantId) {
        this.variantId = variantId;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getOrganization_name() {
        return organization_name;
    }

    public void setOrganization_name(String organization_name) {
        this.organization_name = organization_name;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getGtin() {
        return gtin;
    }

    public void setGtin(String gtin) {
        this.gtin = gtin;
    }

    public int getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(int shelfLife) {
        this.shelfLife = shelfLife;
    }

    public String getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(String productionDate) {
        this.productionDate = productionDate;
    }

    public int getProductionLocation() {
        return productionLocation;
    }

    public void setProductionLocation(int productionLocation) {
        this.productionLocation = productionLocation;
    }


    /*  the qr code= bit.ly Link+&&+productionDate(6d)+countryId(3d)+versionId(2d)+varientId(3d)+organizationId(6d)
          +productionLocation(3d)+productid(14d)+patchId(6d)+itemId(6d)+shelfLife(4d)+otherId(3d)   */

//    @Override
//    public String toString() {
//        return
//                "&&"+
//                        String.format("%06d%03d%02d%03d%06d%03d%014d%06d%06d%04d%03d",
//                                productionDate,countryId, versionId, variantId,orgId, productionLocation, productId, batchId, itemID, shelfLife, otherId)
//                        String.format("%06d", productionDate)
//                +String.format("%03d", countryId)
//                +String.format("%02d", versionId)
//                +String.format("%03d", variantId)
//                +String.format("%06d",orgId)
//                +String.format("%03d", productionLocation)
//                + String.format("%014d", productId) +String.format("%06d", batchId)
//                +String.format("%06d", itemID)
//                + String.format("%04d", shelfLife)+String.format("%03d", otherId)
//                ;
//    }
}
