package com.example.entity.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name ="transaction")
public class TransactionModel{

@Id
//@GeneratedValue(strategy = GenerationType.TABLE)
    private long id;
    private long count;
    private String production_date;
    private String transaction_type ;
    private int production_profile;
    private int username;
    @OneToMany(mappedBy = "transactionModel",cascade = CascadeType.ALL ,fetch = FetchType.LAZY)
    @JsonIgnoreProperties("transactionModel")
    private List<MalyaanCodeModel>malyaanCodeModels ;

    public TransactionModel() {
    }

    public TransactionModel(long id, long count, String production_date) {
        this.id = id;
        this.count = count;
        this.production_date = production_date;
    }

    public TransactionModel(long id, long count, String production_date, String transaction_type, int production_profile, int username, List<MalyaanCodeModel> malyaanCodeModels) {
        this.id = id;
        this.count = count;
        this.production_date = production_date;
        this.transaction_type = transaction_type;
        this.production_profile = production_profile;
        this.username = username;
        this.malyaanCodeModels = malyaanCodeModels;
    }

    public List<MalyaanCodeModel> getMalyaanCodeModels() {
        return malyaanCodeModels;
    }

    public void setMalyaanCodeModels(List<MalyaanCodeModel> malyaanCodeModels) {
        this.malyaanCodeModels = malyaanCodeModels;
    }

    public TransactionModel(long count) {
        this.count = count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
