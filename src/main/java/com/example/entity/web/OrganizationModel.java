package com.example.entity.web;


import javax.persistence.*;

@Entity
@Table(name = "organization",schema = "organization_schema")
public class OrganizationModel {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    private String name;


    private String specification ;
    private String image_url ;
    private String balance ;
    private String  qr_price;
    private String download ;

    public OrganizationModel() {
    }

    public OrganizationModel(String name, String specification, String image_url, String balance, String qr_price, String download) {
        this.name = name;
        this.specification = specification;
        this.image_url = image_url;
        this.balance = balance;
        this.qr_price = qr_price;
        this.download = download;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getQr_price() {
        return qr_price;
    }

    public void setQr_price(String qr_price) {
        this.qr_price = qr_price;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }
}
