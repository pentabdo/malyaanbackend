package com.example.entity.web;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product_profile")
public class ProductModel {
    @Id
    private long id;
   private int loyality_point;
   private String last_modification_date;
   private int last_modification_user_id;
   private String product_code ;
   private String product_json;
   private String log_date;
   private int user_id;
   private int form_id;

    public ProductModel() {
    }

    public ProductModel( int loyality_point, String last_modification_date, int last_modification_user_id, String product_code, String product_json, String log_date, int user_id, int form_id) {
        this.loyality_point = loyality_point;
        this.last_modification_date = last_modification_date;
        this.last_modification_user_id = last_modification_user_id;
        this.product_code = product_code;
        this.product_json = product_json;
        this.log_date = log_date;
        this.user_id = user_id;
        this.form_id = form_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getLoyality_point() {
        return loyality_point;
    }

    public void setLoyality_point(int loyality_point) {
        this.loyality_point = loyality_point;
    }

    public String getLast_modification_date() {
        return last_modification_date;
    }

    public void setLast_modification_date(String last_modification_date) {
        this.last_modification_date = last_modification_date;
    }

    public int getLast_modification_user_id() {
        return last_modification_user_id;
    }

    public void setLast_modification_user_id(int last_modification_user_id) {
        this.last_modification_user_id = last_modification_user_id;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getProduct_json() {
        return product_json;
    }

    public void setProduct_json(String product_json) {
        this.product_json = product_json;
    }

    public String getLog_date() {
        return log_date;
    }

    public void setLog_date(String log_date) {
        this.log_date = log_date;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getForm_id() {
        return form_id;
    }

    public void setForm_id(int form_id) {
        this.form_id = form_id;
    }

}
