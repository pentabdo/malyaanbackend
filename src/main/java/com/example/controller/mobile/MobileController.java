package com.example.controller.mobile;

import com.example.dto.mobile.ProductCheckRequest;
import com.example.dto.mobile.RegesterRequest;
import com.example.entity.mobile.BToCUser;
import com.example.entity.mobile.OrganizationInformation;
import com.example.entity.web.MalyaanCodeModel;
import com.example.entity.web.OrganizationModel;
import com.example.repository.mobile.MobileUserRepository;
import com.example.repository.web.MalyaanCodeRepo;
import com.example.repository.web.OrganizationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("mobile")
public class MobileController {

    @Autowired
    private MobileUserRepository userRepository;

    @Autowired
    private OrganizationRepo organizationRepo;

    @Autowired
    private MalyaanCodeRepo malyaanCodeRepo;

    @PostMapping("/loginRegister")
    public String loginRegister(@RequestBody RegesterRequest regesterRequest)
    {
        String id ="";

        if(userRepository.findByPhoneNumber(regesterRequest.getPhone())==null)
        {
            BToCUser user = new BToCUser(regesterRequest.getPhone());
            userRepository.save(user);
            id=String.valueOf(user.getId());
        }
        else {

            BToCUser registerdUser =userRepository.findByPhoneNumber(regesterRequest.getPhone());
            id = String.valueOf(registerdUser.getId());}

        return id;
    }
    
    @PostMapping("/getAllOrgInfo")
    public List<OrganizationInformation> getAllOrganizations() 
    {
        List<OrganizationInformation> returned=new ArrayList<>();
        List<OrganizationModel> all = organizationRepo.findAll();
        for (OrganizationModel org : all)
        {
            OrganizationInformation or = new OrganizationInformation(org.getId(),org.getName());
            returned.add(or);
        }

        return returned;
    }


    @PostMapping
    public String getQrOrganization(@RequestParam String id)
    {
        OrganizationModel model = organizationRepo.findOne(Long.valueOf(id));
        return model.getName();
    }

    @PostMapping("/checkproduct")
    public void checkproduct(@RequestBody ProductCheckRequest productCheckRequest)
    {

        MalyaanCodeModel malyaanCodeModel = malyaanCodeRepo.findByQrcode(productCheckRequest.getQr());
        malyaanCodeModel.setIsbought(true);
        malyaanCodeModel.setScanned_date(String.valueOf(new Date()));
        malyaanCodeModel.setUser_id(productCheckRequest.getId());
        malyaanCodeRepo.save(malyaanCodeModel);

    }

    @PostMapping("/checkallproduct")
    public void checkAllProduct(@RequestBody Set<ProductCheckRequest> productCheckRequests)
    {
        for(ProductCheckRequest product:productCheckRequests) {
            MalyaanCodeModel malyaanCodeModel = malyaanCodeRepo.findByQrcode(product.getQr());
            malyaanCodeModel.setIsbought(true);
            malyaanCodeModel.setScanned_date(String.valueOf(new Date()));
            malyaanCodeModel.setUser_id(product.getId());
            malyaanCodeRepo.save(malyaanCodeModel);
        }


    }





}
