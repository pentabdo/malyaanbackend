package com.example.controller;

import com.example.entity.web.*;
import com.example.repository.web.MalyaanCodeRepo;
import com.example.repository.web.OrganizationRepo;
import com.example.repository.web.ProductModelRepository;
import com.example.repository.web.TransactionRepo;
import com.example.service.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;




@RestController
@RequestMapping("web")
public class WebController {

    @Autowired
    private OrganizationRepo organizationRepo;
    @Autowired
    private TransactionRepo transactionRepo;
    @Autowired
    private ProductModelRepository productModelRepository;
    @Autowired
    private WebService webService;
    @Autowired
    private MalyaanCodeRepo malyaanCodeRepo;


    @PostMapping("/generateTransaction")
    public String generateTransaction(@RequestBody QrCodeModel codeModel )
    {
        OrganizationModel organizationModel = organizationRepo.findByName(codeModel.getOrganization_name());
        ProductModel productModel =productModelRepository.findOne(codeModel.getProductId());
        long count = codeModel.getCount();
        long lastbatchid = transactionRepo.getMaxId();
        long currentBtchID = lastbatchid+1;
        String date = webService.dateFormating(codeModel.getProductionDate());
        TransactionModel transactionModel = new TransactionModel(currentBtchID,count,codeModel.getProductionDate());
        List<MalyaanCodeModel> codeModels = new ArrayList<MalyaanCodeModel>();
        StringBuilder fullqr = new StringBuilder("");
        for(long i = 0 ;i<count ;i++ )
        {
            String qr = webService.backQrGenerator(date,codeModel.getCountryId(),1,codeModel.getVariantId(),organizationModel.getId(),
                    codeModel.getProductionLocation(),codeModel.getProductId(),currentBtchID,i+1,codeModel.getShelfLife(),productModel.getLoyality_point());
            MalyaanCodeModel malyaanCodeModel=new MalyaanCodeModel(qr ,transactionModel,false,false);
            fullqr.append(qr);
            if(i<count-1) {
                fullqr.append("*");
            }
            codeModels.add(malyaanCodeModel);
        }



        transactionModel.setMalyaanCodeModels(codeModels);
        transactionRepo.save(transactionModel);
        return fullqr.toString();
    }

    @PostMapping("/generateMalyaan")
    public String generateMalyaan(@RequestBody QrCodeModel codeModel )
    {
        OrganizationModel organizationModel = organizationRepo.findByName(codeModel.getOrganization_name());
        ProductModel productModel =productModelRepository.findOne(codeModel.getProductId());
        long count = codeModel.getCount();
        long lastbatchid = transactionRepo.getMaxId();
        long currentBtchID = lastbatchid+1;
        String date = webService.dateFormating(codeModel.getProductionDate());
        TransactionModel transactionModel = new TransactionModel(currentBtchID,count,codeModel.getProductionDate());
        transactionRepo.save(transactionModel);
        List<MalyaanCodeModel> codeModels = new ArrayList<MalyaanCodeModel>();
        StringBuilder fullqr = new StringBuilder("");
        for(long i = 0 ;i<count ;i++ )
        {
            String qr = webService.backQrGenerator(date,codeModel.getCountryId(),1,codeModel.getVariantId(),organizationModel.getId(),
                    codeModel.getProductionLocation(),codeModel.getProductId(),currentBtchID,i+1,codeModel.getShelfLife(),productModel.getLoyality_point());
            MalyaanCodeModel malyaanCodeModel=new MalyaanCodeModel(qr ,transactionModel,false,false);
            fullqr.append(qr);
            if(i<count-1) {
                fullqr.append("*");
            }
            codeModels.add(malyaanCodeModel);
        }



        malyaanCodeRepo.save(codeModels);
//        transactionModel.setMalyaanCodeModels(codeModels);

        return fullqr.toString();
    }


    @GetMapping("/getAllTrans")
    public List<TransactionModel> getAll()
    {
        return transactionRepo.findAll();
    }


}
