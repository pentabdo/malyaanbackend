package com.example.repository.mobile;

import com.example.entity.mobile.BToCUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MobileUserRepository extends JpaRepository<BToCUser,Long> {

    public BToCUser findByPhoneNumber(String number);
}
