package com.example.repository.mobile;

import com.example.entity.mobile.UserAction;
import com.example.entity.mobile.UserActionId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserActionRepository extends JpaRepository<UserAction,UserActionId> {

}
