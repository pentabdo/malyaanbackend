package com.example.repository.mobile;

import com.example.entity.mobile.Action;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActionRepository extends JpaRepository<Action,Long>{

}
