package com.example.repository.mobile;

import com.example.entity.mobile.App;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppRepository extends JpaRepository<App,Long> {
}
