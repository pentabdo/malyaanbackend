package com.example.repository.web;

import com.example.entity.web.TransactionModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TransactionRepo extends JpaRepository<TransactionModel,Long> {

    @Query("SELECT coalesce(max(ch.id), 0) FROM TransactionModel ch")
    Long getMaxId();
}
