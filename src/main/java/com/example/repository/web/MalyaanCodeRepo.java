package com.example.repository.web;

import com.example.entity.web.MalyaanCodeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MalyaanCodeRepo extends JpaRepository<MalyaanCodeModel,Long> {

    @Query("SELECT coalesce(max(ch.id), 0) FROM MalyaanCodeModel ch")
    Long getMaxId();

    MalyaanCodeModel findByQrcode(String qr);


}
