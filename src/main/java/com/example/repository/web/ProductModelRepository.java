package com.example.repository.web;

import com.example.entity.web.ProductModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductModelRepository extends JpaRepository<ProductModel,Long> {



}
