package com.example.repository.web;

import com.example.entity.web.OrganizationModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrganizationRepo extends JpaRepository<OrganizationModel,Long> {
    public OrganizationModel findByName(String name);
}
