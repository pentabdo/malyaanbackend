package com.example.dto.mobile;

public class ProductCheckRequest {

    private String action;
    private String app;
    private String id;
    private String qr;
    private String date;
    public ProductCheckRequest() {
    }

    public ProductCheckRequest(String action, String app, String qr) {
        this.action = action;
        this.app = app;
        this.qr = qr;
    }

    public ProductCheckRequest(String action, String app, String qr, String date) {
        this.action = action;
        this.app = app;
        this.qr = qr;
        this.date = date;
    }

    public ProductCheckRequest(String action, String app, String id, String qr, String date) {
        this.action = action;
        this.app = app;
        this.id = id;
        this.qr = qr;
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
