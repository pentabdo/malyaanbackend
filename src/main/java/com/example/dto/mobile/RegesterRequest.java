package com.example.dto.mobile;

public class RegesterRequest  {

    private String action;
    private String app;
    private String phone;

    public RegesterRequest() {
    }

    public RegesterRequest(String phone) {
        this.phone = phone;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
